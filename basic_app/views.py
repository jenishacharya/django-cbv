from django.shortcuts import render
from django.urls import reverse_lazy
# a eneric type of class based view
from django.views.generic import (View, TemplateView, ListView, DetailView, 
                                    CreateView, UpdateView, DeleteView)
from django.http import HttpResponse
from basic_app import models
# Create your views here.

def index(request):
    my_dictionary = {'value': 'new_value'}
    return render(request, 'index.html', context=my_dictionary)

class CBView(View):
    def get(self, request):
        return HttpResponse("Class based view, first use!")

class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['value'] = 'value for context'
        context['up_value'] = 'updated value for context'
        return context

class SchoolListView(ListView):
    # when no context_object_name is set, ListView by default, passes an object by the 
    # name of lowercase model name '_' list(school_list in this case)
    # for DetailView, the name is lowercase value of model name
    model = models.School
    template_name = 'basic_app/school_list.html'

class SchoolDetailView(DetailView):
    context_object_name = 'school_detail'
    model = models.School
    template_name = 'basic_app/school_detail.html'

class SchoolCreateView(CreateView):
    model = models.School
    # without adding fields, we get an ImproperlyConfigured error
    fields = ('name', 'principal', 'location')


class SchoolUpdateView(UpdateView):
    model = models.School
    fields = ('name', 'principal')


class SchoolDeleteView(DeleteView):
    model = models.School
    # once the school has been deleted, go back to this page
    # we use reverse_lazy as we only want this to be called as deletion is successful
    success_url = reverse_lazy("basic_app:list")